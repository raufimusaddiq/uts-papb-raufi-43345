package pre.com.precafpapb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class HompageActivity extends AppCompatActivity {
    private CardView cardViewManajemenPegawai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hompage);

        cardViewManajemenPegawai = findViewById(R.id.cardViewPegawai);
        cardViewManajemenPegawai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PegawaiActivity.class);
                startActivity(intent);
            }
        });

    }
}
